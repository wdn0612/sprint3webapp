package com.hack.controllers;

import com.hack.entities.User;
import com.hack.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Slf4j
@Controller
public class AdminController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping ("/admin")
    public String handleAdminLogin(Model model){
        model.addAttribute("admin",new User());
        log.info("Switched to admin login page.");
        log.info(String.valueOf(userRepository.findAll()));
        return "adminlogin";
    }

    @PostMapping("/admin-login")
    public String handleAdminLoginResults(@ModelAttribute User admin, Model model){
        if (admin.getName().equals("admin") && admin.getEmail().equals("c0nygre.com")){
            log.info("Admin credentials matched! Redirecting to userdatabase page...");
            return handleAdminHome(model);
        }
        else{
            log.info("Admin credentials unmatched.");
            return "error";
        }

    }

    @GetMapping("/admin-login")
    public String handleAdminHome (Model model){
        model.addAttribute("Customers", userRepository.findAll());
        model.addAttribute("createUser",new User());
        model.addAttribute("updateUser",new User());
        model.addAttribute("userIDs", userRepository.getAllIds());
        return "userinformation";
    }

    @PostMapping("/admin-login/create-user")
    public String handleCreate (Model model, @ModelAttribute User createdUser){
        userRepository.save(createdUser);
        return handleAdminHome(model);
    }

    @PostMapping("/admin-login/update-user")
    public String handleUpdate (Model model, @RequestParam(value="customer_ID") long user_ID, @ModelAttribute User updatedUser){
        updatedUser.setId(user_ID);
        userRepository.save(updatedUser);
        return handleAdminHome(model);
    }

    @PostMapping("/admin-login/delete-user")
    public String handleDelete (Model model, @RequestParam(value="delete_ID") int deleted_user){
        userRepository.deleteById(deleted_user);
        return handleAdminHome(model);
    }

}

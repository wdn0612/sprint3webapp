package com.hack.controllers;

import com.hack.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import com.hack.entities.User;

@Slf4j
@Controller
public class RegisterController {


    @Autowired
    private UserRepository userRepository;

    private long nextCustomerID;

    @PostMapping(value = "/register", params = "register")
    public String handleRegister(Model model) {
        model.addAttribute("newUser", new User());
        return "register";
    }

    @PostMapping(value = "/register-user")
    public String registerUser(@ModelAttribute User newCustomer, Model model){
        log.info("Registering for user {} with password {}",
                newCustomer.getName(),newCustomer.getEmail());
        if (userRepository.existsByName(newCustomer.getName())){
            log.info("Failed to register for {} as user already exists.", newCustomer.getName());
            return "existinguser";
        }
        userRepository.save(newCustomer);
        log.info("New user saved!");
        return "success";
    }
}


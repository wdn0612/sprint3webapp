package com.hack.controllers;

import com.hack.entities.User;
import com.hack.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/")
    public String home(Model model) {
        log.info("Switched to user login page.");
        model.addAttribute("newCustomer", new User());
      //  model.addAttribute("Customers", Repository.getAllCustomers());
        return "home";
    }

    @PostMapping(value = "/login", params = "login")
    public String handleLogin(@ModelAttribute User newCustomer, Model model) {
        log.info("User login with Name: {} and EmailAddress: {}",
                newCustomer.getName(), newCustomer.getEmail());
        try {
            User dbCustomer = userRepository.findByName(newCustomer.getName());
            if (newCustomer.getEmail().equals(dbCustomer.getEmail())) {
                log.info("User successfully logged in.");
                return "success"; // switch to stock query page
            } else {
                log.info("Username and password do not match.");
                return "error";
            }
        }catch (NullPointerException e){
            log.info("Database does not have this user.");
            return "error";
        }
    }

}





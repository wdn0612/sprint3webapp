package com.hack;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import static com.fasterxml.jackson.databind.type.LogicalType.DateTime;

@Controller
@CrossOrigin

public class FinanceAPI {

    @GetMapping(value="/yahooAPI")
    public String getYahoo() {
        return "index";
    }

    public static String getAPIPath(){
        String currentPath= System.getProperty("user.dir");
        Path filepath =  Paths.get(currentPath,"src","main","resources","apikey.txt");
        return  filepath.toString();
    }


    private String apifilename= FinanceAPI.getAPIPath();

    @GetMapping(value="/stock/{types}/{symbol}", produces = {"application/json"})
    public String getStockData(@PathVariable String types, @PathVariable String symbol){
        HashMap<String,String> entries= new HashMap<>();
        AlphaVintageAPI api =null;
        try {
            api = new AlphaVintageAPI(apifilename);
        }
        catch (FileNotFoundException fE){
            System.out.println("File not found ");
        }
        JSONObject data= api.getStock(symbol);

        Iterator<String> keys = data.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            try {
                String type_string= FinanceAPI.getStockPriceType(types);
                String entry= data.getJSONObject(key).get(type_string).toString() ;
                entries.put(key,entry);


            }catch (JSONException e){
                System.out.println("JSON Exception ");

            }
        }

        System.out.println(entries);
        return entries.toString();
    }

    @GetMapping(value="/crypto/{types}/{symbol}", produces = {"application/json"})
    public ResponseEntity<String> getCryptoData(@PathVariable String types, @PathVariable String symbol){
        AlphaVintageAPI api =null;
        try {
            api = new AlphaVintageAPI(apifilename);
        }
        catch (FileNotFoundException fE){
            System.out.println("File not found ");
        }
        try {
            Instant now = Instant.now(); //current date
            Instant before = now.minus(Duration.ofDays(300));
            Date dateBefore = Date.from(before);
            String date = new SimpleDateFormat("yyyy-MM-dd").format(dateBefore);
            JSONObject data = api.getCrypto(symbol, date);
            String type_string = FinanceAPI.getCryptoPricetypes(types);
            String entry = data.get(type_string).toString();
            System.out.println(entry);
            String r_msg=symbol +":"+ types+ " USD"+entry;
            return new ResponseEntity<>(r_msg,HttpStatus.OK);

        }
        catch (JSONException e){
            System.out.println("JSON exception");
        }
        return ResponseEntity.status(HttpStatus.OK).header("Crypto Price","String").body("");

    }
    public static String getCryptoPricetypes(String type){
        switch (type){
            case "high":
                return "2b. high (USD)";

            case "low":
                return "3b. low (USD)";

            case "close":
                return "4. close";
            case "open":
                return"1b. open (USD)";

        }
        return "";
    }

    public static String getStockPriceType(String type ){
        switch (type){
            case "high":
                return "2. high";

            case "low":
                return "3. low";

            case "close":
                return "4. close";
            case "volume":
                return"5. volume";

        }
        return "";
    }
}
